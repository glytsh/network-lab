# Network-Lab

# TP n°2 - Stockage :

## 1. Choix de la technologie de stockage
Pour ce TP j'ai choisi 2 technologies de stockages:
- iSCSI (Internet Small Computer System Interface). Deuxième protocole SAN ou par blocs le plus répandu (10 % à 15 % du marché), iSCSI encapsule les commandes SCSI dans des trames Ethernet et utilise Ethernet IP pour le transport.
- NFS (Network File System). À l'origine, NFS a été développé pour les environnements de serveurs UNIX. Il est également un protocole Linux courant.

Ce choix technologie a été fait car je voulais toucher des stockages NAS (NFS) et SAN(iSCSI).
Il est donc tout naturel que je me sois tourné vers les technologies les plus répendues du marché.   
Et également ça me permet de toucher deux secteurs différents. 
Un SAN étant plus adapté à une démarche professionnelle et un NAS plus adapté pour un particulier. 

Pour le SAN j'ai choisi d'utiliser TrueNAS (Anciennement FreeNAS) et pour le NAS j'ai décidé d'utiliser un Windows Server 2016 mais ça aurait tout aussi bien fonctionné avec un Windows 10 basique.

**Architecture** :  
| ESXi              | Windows Server  | TrueNAS         |
| :-----            | :----:          | -----:          |
| 192.168.223.129   | 192.168.223.128 | 192.168.223.134 |



**Avantages/Inconvénients des protocoles utilisés** :

iSCSI :
- Avantages : 
    - Utilisation du réseau Ethernet
    - Coûts maitrisés face au « Fiber Channel »
    - Assez Fléxible sur le matériel, facilement déployable
    - Technologie très utilisée
- Inconvenients :
    - Certains matériels / OS / systèmes ont une configuration iSCSi assez ardue à mettre en place
    - Configuration automatique du lien iSCSI pour accéder aux données, que se soit sur le client que sur le support de stockage

NFS : 
- Avantages : 
    - Très simple utilisation
    - Prix très avantageux 
    - Evolutif
- Inconvenients :
    - Lent, peu adapté a un role professionel 



## 2. Mise en place du stockage

### **Installation de TrueNAS** :

Prérequis : 
- minimum 8 GB RAM
- Disque dur
- ISO TrueNAS

1. Installation de TrueNAS sur VM 
- Créer la VM sur ton hyperviseur favoris
- Demarrer la VM avec l'iso TrueNAS core
- Une fois booté sur l'iso presser la touche Enter [image ci-dessous]
![mise en service](./images/Installation-TrueNAS.png)  
- Presser 1 Install/upgrade  
![mise en service](./images/Installation-FreeNAS-Install.png)
- Choisir la destination  
![mise en service](./images/Installation-FreeNAS-Destination.png)
- Confirmer  
![mise en service](./images/Installation-FreeNAS-Confirmation.png)
- Créer le mot de passe pour le Root  
![mise en service](./images/Installation-FreeNAS-root.png)
- Choisir la méthode de boot (BIOS recommandé)  
![mise en service](./images/Installation-FreeNAS-BIOS.png)
- Confirmation  
![mise en service](./images/Installation-FreeNAS-End.png)

### **iSCSI** : 

Pour expliquer le fonctionnement :  
La technologie iSCSI transporte des blocs de données entre un initiateur iSCSI, installé sur un serveur, et une cible iSCSI, installée sur un dispositif de stockage.

Mise en place sur TrueNAS :

1. **Activation du service**  
 Pour commencer il faut démarrer le service.
donc dans **service** > **iSCSI**
![mise en service](./images/etape1-service.png)

2. **Déployer un partage iSCSI**  
Après avoir activé le service nous allons créer un partage iSCSI.  
Allez dans **Sharing** > **Block Shares (iSCSI)**  
Et cliquez sur **wizard**  
![déploiement](./images/iSCSIWizard.png)

3. **Création du block**  
Commence par entrez le nom de votre block.  
Ensuite tu devra choisir entre un dossier(file) ou un périphérique(device).
- File : Selectionne le chemin du dossier et précise la taille que tu veux y attribuer.
- Device : Selectionne le périphérique que tu veux partager. 
Ensuite le type de plateforme.
![block](./images/iSCSICreateBlockDevice.png)

4. **Portail**  
Le portail permet de se connecter au stockage.  
Lors de la creation tu devra choisir le type d'authentification.
Créer un groupe avec son ID et les utilisateurs attribués et un mot de passe.  
Par défault le portail utilise l'addresse ip du serveur.
![portail](./images/iSCSIPortal.png)

5. **Initiateur**  
Tu peux si besoin autoriser des initiateurs particuliers. Mais tu peux laisser les cases vides 

6. **Confirmer**  
Confirme et bravo t'as initié ton SAN.

### **NFS** :

1. Installation 

- Dans l’Assistant Ajout de rôles et fonctionnalités, sous Rôles de serveurs, sélectionnez Services de fichiers et de stockage si ce composant n’a pas déjà été installé.
- Sous services de fichiers et iSCSI, sélectionnez serveur de fichiers et serveur pour NFS.Sélectionnez Ajouter des fonctionnalités pour inclure les fonctionnalités NFS sélectionnées.
- Sélectionnez installer pour installer les composants NFS sur le serveur.

2. Configuration  
- Ouvrez une session sur le serveur en tant que membre du groupe Administrateurs local.
- Le Gestionnaire de serveur démarre automatiquement. S’il ne démarre pas automatiquement, sélectionnez Démarrer, tapez servermanager.exe, puis sélectionnez Gestionnaire de serveur.
- Sur la gauche, sélectionnez services de fichiers et de stockage, puis sélectionnez partages.
- Sélectionnez cette option pour créer un partage de fichiers, puis démarrez l’assistant nouveau partage.
- Dans la page Sélectionner un profil , sélectionnez partage NFS : rapide ou partage NFS-avancé, puis sélectionnez suivant.
- Sur la page emplacement du partage , sélectionnez un serveur et un volume, puis sélectionnez suivant.
- Dans la page nom du partage , spécifiez un nom pour le nouveau partage, puis sélectionnez suivant.
- Dans la page authentification , spécifiez la méthode d’authentification que vous souhaitez utiliser pour ce partage.
- Sur la page partager les autorisations , sélectionnez Ajouter, puis spécifiez l’ordinateur hôte, le groupe de clients ou le groupe auquel vous souhaitez accorder l’autorisation d’accès au partage.
- Dans autorisations, configurez le type de contrôle d’accès que les utilisateurs doivent avoir, puis sélectionnez OK.
- Dans la page confirmation , passez en revue votre configuration, puis sélectionnez créer pour créer le partage de fichiers NFS.

## 3. Mise en fonctionnement avec l'ESXi

Les étapes de liaisons sur mon serveur ESXi pour les deux technologies.

### **iSCSI**

1. Créer un switch virtuel : **clic droit Network**>**New Virtual switch** : 
    - Choisi un nom pour ton vSwitch
    ![block](./images/Step1.png)
    - Choisi ton NIC physique
    ![block](./images/Step2.png)
    - Clique sur ajouter et sauvegarder 

2. Créer un VMkernel NIC :
    - Choisi ton Port group 
    - Choisi le vSwitch créer précédement
    - Aggrandi la partie IP et clique sur Static
    - Configure l'ip et le masque
    - Clique sur Create  
    ![block](./images/NIC.png)
3. Vérifie la création de ce que tu viens de créer (vSwitch & VMkernel NIC)
4. Activer l'adaptateur dans stockage :
    - Dans stockage trouver adaptator 
    - Ensuite cliquer sur configuration iSCSI 
    - Cochez "enable"
5. Configurer l'adaptateur : 
    - Après avoir cocher enable, ajouter l'addresse IP de votre SAN dans les cibles dynamiques
    - Vérifier que votre SAN apparaisse dans cible static en confirmant et en revenant sur la config
    ![block](./images/adaptater.png)
6. Créer un datastore : 
    - Sur le panel créer un nouveau datastore
    ![block](./images/DS1.png)
    - Créer un datastore VMFS
    ![block](./images/DS2.png)
    - Selectionner la cible et ajouter un nom au datastore
    ![block](./images/DS3.png)
    - Allouer la partition voulu
    ![block](./images/DS4.png)
    - Confirmer
    - Vérifier la création du datastore 

### **NFS**

Afin d'ajouter un stockage NFS sur ESXi c'est très simple.
1. Créer un stockage et cocher Network File System 
2. Renseigner l'ip du server et le chemin du fichier partagé
3. Cliquer sur Valider
4. Vérifier dans votre serveur que le stockage existe et est connecté.
